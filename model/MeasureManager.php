<?php
namespace model;

class MeasureManager extends DatabaseConnection{

    public function saveMeasure(Measure $measure) {
        $query = $this->bdd->prepare
        ("INSERT INTO measure(datetime, temperature, humidity) VALUES(:datetime, :temperature, :humidity)");

        $query->bindValue(":datetime", $measure->getDatetime());
        $query->bindValue(":temperature", $measure->getTemperature());
        $query->bindValue(":humidity", $measure->getHumidity());

        $query->execute();

        $id = $this->bdd->lastInsertId();
        $measure->setId($id) ;

        return $id;
    }

    public function getAllMeasure() {
        $result = [];

        $query = $this->bdd->query("SELECT id, datetime, temperature, humidity FROM measure ORDER BY id DESC");

        while($data_list = $query->fetch()) {
            $datetime = $data_list['datetime'];
            $temperature = $data_list['temperature'];
            $humidity = $data_list['humidity'];

            $measure = new Measure($datetime, $temperature, $humidity);
            $measure->setId($data_list['id']);

            $measures[] = $measure;
        }

        return $measures;
    }

    public function getMeasureById($id) {
        $result = null;

        $query = $this->bdd->prepare("SELECT id, datetime, temperature, humidity FROM measure WHERE id = :id");

        $query->bindParam(":id", $id);

        if($query->execute()) {
            if($data = $query->fetch()) {
                $datetime = $data['datetime'];
                $temperature = $data['temperature'];
                $humidity = $data['humidity'];

                $measure = new Measure($datetime, $temperature, $humidity);
                $measure->setId($data['id']);
            }
        }

        return $measure;
    }

    public function getLastMeasure() {
        $result = null;

        $query = $this->bdd->prepare("SELECT id, datetime, temperature, humidity FROM measure ORDER BY datetime DESC LIMIT 1");

        if($query->execute()) {
            if($data = $query->fetch()) {
                $id = $data['id'];
                $datetime = $data['datetime'];
                $temperature = $data['temperature'];
                $humidity = $data['humidity'];

                $result = new Measure($datetime, $temperature, $humidity);
                $result->setId($id) ;
            }
        }

        return $result;
    }

    public function updateMeasure($measure, $id) {
        $query = $this->bdd->prepare
        ("UPDATE measure SET datetime = :datetime, temperature = :temperature, humidity = :humidity WHERE id = :id");

        $query->bindParam(":datetime", $measure->datetime);
        $query->bindParam(":temperature", $measure->temperature);
        $query->bindParam(":humidity", $measure->humidity);

        $query->bindParam(":id", $id);

        $query->execute();
    }

    public function deleteMeasure($id) {
        $query = $this->bdd->prepare("DELETE FROM measure WHERE id = :id");

        $query->bindParam(":id", $id);

        $query->execute();
    }

}

