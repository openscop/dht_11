<?php
namespace view;

class Page
{

    private $title = "";
    private $block_body = array();   // html body content

    function __construct($title)
    {

        $this->title = $title;
    }

    public function addBlockToBodyWithTemplate($templateName, $dataArray)
    {
        $this->block_body[] = array('templateName' => $templateName, 'data' => $dataArray);
    }

    public function render()
    {
        global $errors;
        ob_start();
        require_once('view/template/page.phtml');
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

}
